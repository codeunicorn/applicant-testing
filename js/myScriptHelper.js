function myHelper() {

    this.hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"); 

    //Function to convert rgb color to hex format (without the '#')
    this.rgb2hex = function(rgb) {
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        return this.hex(rgb[1]) + this.hex(rgb[2]) + this.hex(rgb[3]);
    }
   
   this.hex = function(x) {
     return isNaN(x) ? "00" : this.hexDigits[(x - x % 16) / 16] + this.hexDigits[x % 16];
    }

    /**
     * Function to convert an integer to hex format
     */
    this.intToHex = function(your_number) {
        hex_string = your_number.toString(16);
        return hex_string;
    };

    /**
     * Function to convert a hex number (without the #) into an integer
     */
    this.hextoInt = function(hex_num) {
        your_number = parseInt(hex_num, 16);
        return your_number;
    };



    /**
     * Function for Step 4. There is an error with this function
     */
    this.pushNext = function(element) {
        var children = document.getElementById('btn_section').children;
        var len = children.length;

        var last_child = $('#'+ children[len - 1].id),
            the_element = $('#'+ children[0].id);

        $(last_child).insertBefore(the_element);
    }
}