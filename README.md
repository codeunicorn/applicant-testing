# Code Unicorn Application Test

Thank you for showing an interest in joining Code Unicorn! This is a small exercise designed to test your experience in HTML/CSS/Javascript. Don't worry, it won't be too hard! If you need some clarification with something, don't hesitate to reach out to your contact at Code Unicorn.


## Start

To start, you'll need to copy the files here to your computer. If you know git, you can clone this repository with the [Clone] button on the top right.
Or you can download the files by clicking on "Downloads" on the left menu (or just click on the link below)
 
**[Download Files Here](https://bitbucket.org/codeunicorn/applicant-testing/downloads/)**

Once you've got them downloaded to your computer, you should be able to open up index.html on your browser (we use Chrome, so it's best if you use that too).


## Step 1 [HTML]: Add missing HTML content

Open `index.html` on your browser and the `screenshot.png` image on the folder. You'll notice that towards the middle of the page, there's a section missing below the `Leica Zeno 20`
where the `Leica GGo4 plus` section should be. Your first task is to edit the `index.html` file and add that missing section.

#### Hint:
Look at the section for the `Leica Zeno 20` and you'll notice that they look very similar. The image for the GG04 can be found in the `/img` folder.


## Step 2 [HTML/CSS]: Fix form on header

If you take a look at the form on the header, you'll notice that it's just an image. Replace that image with a real form and form fields (the form doesn't have to submit)

#### Hint:
This uses the [bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/) template. You should be able to use [bootstrap 4 form elements](https://getbootstrap.com/docs/4.0/components/forms/).


## Step 3 [JS]: Background Color Change

The background for the `Leica Zeno 20` and `Leica GG04 plus` sections are green, and we want to change that - Here's what we want to do: Whenever we scroll up/down the page, we want to update the background color with the distance scrolled. 

For example, if we've scrolled 200 pixels down the page, we want to take that 200 and add it to the hexadimal value of the background color, then update the background color (the background should change). To help you out, part of the code has already been written for you. 

**Take a look at the `js/myscript.js` file. You'll add your code where it says `// add your code here to update the background color with the distance scrolled`.**
Helper functions in the file `js/myScriptHelper.js` will help you with conversions between decimal, RBG, and hexadecimal values.

**** Hint:
Convert everything to decimal first - it makes addition/subtraction easier. Then convert it back to hex.


## Step 4 [JS]: Click Order

Take a look at the last section at the bottom of the page. You'll see a navigation menu with the following items:
`Collector` > `Explorer` > `Survey` > `Workforce` > `Navigator` > `AppStudio`

What we want to do is when we click on an item, we want to push everything to the right, and the last item would go in front of the item that we clicked on. 

#### So for example, if we click on `Collector`, the resulting order should be:
Before click:
`Collector` > `Explorer` > `Survey` > `Workforce` > `Navigator` > `AppStudio`
After Click:
 `AppStudio` > `Collector` > `Explorer` > `Survey` > `Workforce` > `Navigator`

#### But if we click on `Workforce`, it should be:
Before click:
`Collector` > `Explorer` > `Survey` > `Workforce` > `Navigator` > `AppStudio`
After Click:
`Collector` > `Explorer` > `Survey` > `AppStudio` > `Workforce` > `Navigator`

We've almost got the code working, but there's an error somewhere. No matter which item we click on, it always pushes everything to the right and puts the last item in the front. 
In `myscript.js`, our click code is:

```javascript
$('.btn_section a').on('click', function(e){
        e.preventDefault();
        helper.pushNext(this);
    });
```
**Your job is to fix the error in the `pushNext()` function located in the `js/myScriptHelper.js` file.** That function is:

```javascript
    this.pushNext = function(element) {
        var children = document.getElementById('btn_section').children;
        var len = children.length;

        var last_child = $('#'+ children[len - 1].id),
            the_element = $('#'+ children[0].id);

        $(last_child).insertBefore(the_element);
    }
```


## You're Done!

Congratulations! You've finished our assessment. Hopefully you didn't find that too difficult. When you're done, you can push your code to the bitbucket repository (if you used git to clone), or you can submit your files via email to [jobs@codeunicorn.com](emailto:jobs@codeunicorn.com). We'll take a few days to look at your code and reach out to you for next steps!


## About

[Code Unicorn](http://codeunicorn.com/) Make Web Magic Happen!
